package epsilon.mail

import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration
import org.springframework.context.annotation.Configuration

@Configuration
class EMailConfiguration : MailSenderAutoConfiguration()