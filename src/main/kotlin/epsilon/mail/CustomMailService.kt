package epsilon.mail

import freemarker.template.Configuration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Component
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils

@Component
class CustomMailService @Autowired constructor(
        private val sender: JavaMailSender,
        private val templates: Configuration) {

    init {
        templates.setClassForTemplateLoading(this.javaClass, "/email-templates/")
    }
    fun send(to: String, from: String, subject: String, text: String) {
        val message = sender.createMimeMessage()
        val helper =  MimeMessageHelper(message, "utf-8")
        helper.setTo(to)
        helper.setFrom(from)
        helper.setSubject(subject)
        helper.setText(text, true)
        sender.send(message)
    }

    fun send(to:String, from: String, subject: String, templateName:String, values:Map<String, String?>)
    {
        val template = templates.getTemplate(templateName)
        val body = FreeMarkerTemplateUtils.processTemplateIntoString(template, values)
        send(to=to, from=from, subject = subject, text = body)
    }
}