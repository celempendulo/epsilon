package epsilon.accounts

import epsilon.ApplicationProperties
import epsilon.accounts.repository.AccountRepository
import epsilon.failure.ClientError
import epsilon.localization.Messages
import epsilon.localization.Tag
import epsilon.mail.CustomMailService
import epsilon.tokens.Token
import epsilon.tokens.TokenManager
import epsilon.validation.Validator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import java.util.*


@Suppress("unused")
@Component
@EnableConfigurationProperties(ApplicationProperties::class)
class AccountManager @Autowired constructor(
        private val repository: AccountRepository,
        private val mailService: CustomMailService,
        private val passwordEncoder: PasswordEncoder,
        private val applicationProperties: ApplicationProperties,
        private val messages: Messages,
        private val tokenManager: TokenManager) {

    fun create(account: Account): Account {
        if (account.id != null && hasAccount(account.id!!)) {
            throw ClientError(messages[Tag.Error.ACCOUNT_ALREADY_EXIST])
        }
        if(account.handler != null){
            if(getByHandler(account.handler!!).isPresent) {
                throw ClientError(messages[Tag.Error.ACCOUNT_HANDLER_TAKEN])
            }
            if(!Validator.isValidHandler(account.handler!!)) {
                throw ClientError(messages[Tag.Error.INVALID_HANDLER_FORMAT])
            }
        }

        if(account.password != null) {
            if(!Validator.isValidPassword(account.password!!)) {
                throw ClientError(messages[Tag.Error.INVALID_PASSWORD_FORMAT])
            }
            account.password = passwordEncoder.encode(account.password)
        }

        if(!account.active && account.email != null) {
            val token = tokenManager.add(account, Token.Type.EMAIL_CONFIRMATION)
            sendVerificationEmail(account, token)
        }
        return repository.save(account)
    }

    fun setEmail(id: String, email: String) {
        val account = forceGet(id)
        account.email = email
        account.emailVerified = false
        val token = tokenManager.add(account, Token.Type.EMAIL_CONFIRMATION)
        sendVerificationEmail(account, token)
        repository.save(account)
    }

    fun update(account:Account): Account {
        if(!repository.existsById(account.id!!)) {
            throw Exception("Account with id ${account.id} does not exist, hence cannot be updated")
        }
        return repository.save(account)
    }

    fun verifyEmail(id: String, code: String, email: String): Account {
        var account = forceGet(id)
        if (account.emailVerified) {
            throw ClientError(messages[Tag.Error.ACCOUNT_ALREADY_ACTIVE])
        }
        tokenManager.validate(account, Token.Type.EMAIL_CONFIRMATION, code)
        account.emailVerified = true
        account = repository.save(account)
        return account
    }

    fun forgotPassword(id: String) {
        val account = forceGet(id)
        if(account.email != null) {
            val token = tokenManager.add(account, Token.Type.PASSWORD_TOKEN)
            sendPasswordResetMail(account, token)
            repository.save(account)
        }
        else {
            throw ClientError(messages[Tag.Error.EMAIL_NOT_SET])
        }
    }

    fun resetPassword(id: String, code: String, newPassword:String) {
        val account = get(id).get()
        if(!Validator.isValidPassword(newPassword)){
            throw ClientError(messages[Tag.Error.INVALID_PASSWORD_FORMAT])
        }
        tokenManager.validate(account,Token.Type.PASSWORD_TOKEN, code)
        account.password = passwordEncoder.encode(newPassword)
        repository.save(account)

    }

    fun get(id: String) : Optional<Account> {
        return repository.findById(id)
    }

    fun getByHandler(handler: String): Optional<Account> {
        return repository.findByHandler(handler)
    }

    fun getByEmail(email: String): Optional<Account> {
        return repository.findByEmail(email)
    }

    fun forceGet(id: String): Account{
        val account = get(id)
        if(!account.isPresent) throw ClientError(messages[Tag.Error.ACCOUNT_NOT_FOUND])
        return account.get()
    }

    fun hasAccount(id: String): Boolean {
        return repository.existsById(id)
    }

    fun delete(id:String) {
        return repository.deleteById(id)
    }

    fun getAll(): MutableIterable<Account> {
        return repository.findAll()
    }

    fun clean() {
        repository.deleteAll()
    }

    private fun sendLink(account: Account, subject: String, url:String, templateName:String) {

        val values = mutableMapOf<String,String?>()
        values["APPLICATION_NAME"]= applicationProperties.name
        values["HANDLER"] = account.handler
        values["URL"]= url
        mailService.send(to=account.email!!,from = applicationProperties.email, subject = subject,
                templateName = templateName, values = values)
    }

    private fun sendVerificationEmail(account: Account, token: Token) {
        val url = applicationProperties.verificationApi.
                replace("code=?", """code=${token.code}""").
                replace("id=?","""id=${account.id}""")
        sendLink(account, messages[Tag.EmailSubject.ACCOUNT_VALIDATION], url, "email-verification.ftl")
    }

    private fun sendPasswordResetMail(account: Account, token: Token)
    {
        val url = applicationProperties.resetPasswordApi.
                replace("code=?", """code=${token.code}""").
                replace("id=?","""id=${account.id}""")
        sendLink(account, messages[Tag.EmailSubject.ACCOUNT_PASSWORD_RESET], url,
                "email-reset-password.ftl")
    }
}