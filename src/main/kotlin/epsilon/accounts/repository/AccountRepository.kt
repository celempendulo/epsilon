package epsilon.accounts.repository

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.accounts.Account
import java.util.*

interface AccountRepository : ArangoRepository<Account, String>{

    fun findByHandler(handler: String): Optional<Account>

    fun findByEmail(email: String): Optional<Account>
}