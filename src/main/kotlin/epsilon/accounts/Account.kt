package epsilon.accounts

import com.arangodb.springframework.annotation.ArangoId
import com.arangodb.springframework.annotation.Document
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import epsilon.localization.Language
import org.springframework.data.annotation.Id
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@JsonIgnoreProperties("password")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Document("accounts")
class Account(var email: String?=null,
              var firstName: String?=null,
              var lastName: String?=null,
              var password: String?=null,
              var nickname: String?= null,
              var handler: String?=null,
              var pp: String? = null,
              var gender: String? = null,
              var dob: Date? = null,
              var active: Boolean = false,
              var blocked: Boolean = false,
              var emailVerified: Boolean = false,
              var roles:List<Roles> = listOf(Roles.USER),
              var languages: List<Language> = listOf(Language.ENGLISH),
              var authInfo: AuthenticationInfo = AuthenticationInfo(),
              @Id var id: String? = null) {

    class AuthenticationInfo(var lastAuthenticateSuccess: LocalDateTime = LocalDateTime.now(),
                             var lastAuthenticateFailure: LocalDate? = null,
                             var authenticateFailureAttempts: Int = 0)

    enum class Roles {
        VISITOR,
        USER,
        PARTNER,
        ADMIN,
        MAINTAINER
    }
}
