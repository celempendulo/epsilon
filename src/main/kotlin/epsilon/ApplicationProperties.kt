package epsilon

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.nio.file.Files
import java.nio.file.Path

@ConstructorBinding
@ConfigurationProperties(prefix = "epsilon.application")
class ApplicationProperties(val name:String="epsilon",
                            val email:String="noreply@epsilon.com",
                            val production:Boolean = true,
                            var mainDirectory: String = "{home}/epsilon",
                            var verificationApi:String="http://localhost:8080/activate-account?id=?&code=?",
                            var resetPasswordApi:String="http://localhost:8080/reset-password?id=?&code=?") {

    init {
        mainDirectory = mainDirectory.replace("{home}", System.getProperty("user.home"))
        if(Files.exists(Path.of(mainDirectory))) {
            Files.createDirectories(Path.of(mainDirectory))
        }
    }

}