package epsilon.security.auth

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "epsilon.security.auth")
class AuthenticationProperties(val loginMaxAttempts: Int =5, val loginAttemptWaitingTime: Int = 30)