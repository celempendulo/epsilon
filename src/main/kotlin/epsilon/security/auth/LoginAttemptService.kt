package epsilon.security.auth

import epsilon.accounts.AccountManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Service
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime

@Service
@EnableConfigurationProperties(AuthenticationProperties::class)
class LoginAttemptService @Autowired constructor(
        private val accountManager: AccountManager,
        private val properties: AuthenticationProperties) {

    fun failed(handler: String) {
        accountManager.getByHandler(handler).ifPresent {
            it.authInfo.authenticateFailureAttempts++
            it.authInfo.lastAuthenticateFailure = LocalDate.now()
            accountManager.update(it)
        }
    }

    fun succeeded(handler: String) {
        accountManager.getByHandler(handler).ifPresent {
            it.authInfo.authenticateFailureAttempts = 0
            it.authInfo.lastAuthenticateFailure = null
            it.authInfo.lastAuthenticateSuccess = LocalDateTime.now()
            accountManager.update(it)
        }
    }

    fun isAttemptExceeded(handler: String): Boolean {
        accountManager.getByHandler(handler).let {
            if(it.isPresent) {
                val account = it.get()
                if(account.authInfo.authenticateFailureAttempts > properties.loginMaxAttempts) {
                    val diff = Duration.between(account.authInfo.lastAuthenticateFailure, LocalDateTime.now())
                    if(diff.toMinutes() < properties.loginAttemptWaitingTime) {
                        return true
                    }
                    else {
                        account.authInfo.authenticateFailureAttempts = 0
                        account.authInfo.lastAuthenticateFailure = null
                        accountManager.update(account)
                    }
                }
            }
        }
        return false
    }
}