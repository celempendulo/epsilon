package epsilon.security.auth

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent
import org.springframework.stereotype.Component

@Component
class AuthenticationFailureListener @Autowired constructor(
        private val loginAttemptService: LoginAttemptService)
    : ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

    override fun onApplicationEvent(event: AuthenticationFailureBadCredentialsEvent) {
        loginAttemptService.failed(event.authentication.name)
    }
}