package epsilon.security.auth

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationSuccessEvent
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Component
class AuthenticationSuccessListener @Autowired constructor(
        private val loginAttemptService: LoginAttemptService)
    : ApplicationListener<AuthenticationSuccessEvent> {

    override fun onApplicationEvent(event: AuthenticationSuccessEvent) {
        loginAttemptService.succeeded(event.authentication.name)
    }
}