package epsilon.security

import epsilon.accounts.Account
import epsilon.accounts.AccountManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService
import org.springframework.security.oauth2.core.OAuth2AuthenticationException
import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.security.oauth2.core.oidc.user.OidcUser
import org.springframework.stereotype.Service


@Service
class CustomOidcUserService @Autowired constructor(
    private val accountManager: AccountManager) : OidcUserService() {

    @Throws(OAuth2AuthenticationException::class)
    override fun loadUser(request: OidcUserRequest): OidcUser {
        val user = super.loadUser(request)
        process(request, user)
        return user
    }

    private fun process(request: OidcUserRequest, user: OidcUser) {
        if (CommonOAuth2Provider.GOOGLE.name.equals(
                request.clientRegistration.clientName,
                ignoreCase = true
            )
        ) {
            if (!accountManager.getByEmail(user.email).isPresent) {
                val account = Account(
                        email = user.email,
                        emailVerified = true,
                        firstName = user.givenName,
                        lastName = user.familyName,
                        pp = user.picture,
                        active = true)
                accountManager.create(account)
            }
            else {
                println("Account already exist")
            }
        } else { // error is not appropriate
            throw OAuth2AuthenticationException(OAuth2Error("invalid provider"))
        }
    }

}