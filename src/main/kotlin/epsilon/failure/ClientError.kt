package epsilon.failure

class ClientError @JvmOverloads constructor(override val message: String, private val code: Int = -1) :
    RuntimeException() {

    constructor(code: Int) : this("something went wrong", code)

    override fun toString(): String {
        return "$message=$code"
    }

}