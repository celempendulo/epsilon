package epsilon.localization

enum class Language(val code: String) {

    AFRIKAANS("afr"),
    ENGLISH("en"),
    ZULU("zul")
}