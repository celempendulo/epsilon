package epsilon.localization

class Tag {
    object Error {
        const val ACCOUNT_ALREADY_EXIST = "error.account.exist"
        const val ACCOUNT_HANDLER_TAKEN = "error.account.handlerTaken"
        const val ACCOUNT_NOT_FOUND = "error.account.notFound"
        const val ACCOUNT_ALREADY_ACTIVE = "error.account.alreadyActive"
        const val ACCOUNT_INCORRECT_CREDENTIALS = "error.account.incorrectCredentials"
        const val INVALID_PASSWORD_FORMAT = "error.account.invalidPasswordFormat"
        const val INVALID_EMAIL_FORMAT = "error.account.invalidEmailFormat"
        const val INVALID_HANDLER_FORMAT = "error.account.invalidHandlerFormat"
        const val INCORRECT_PASSWORD = "error.account.incorrectPassword"
        const val LOGIN_ATTEMPTS_EXCEEDED = "error.account.loginAttemptsExceeded"
        const val PASSWORD_ENTRIES_EXCEEDED = "error.account.passwordEntriesExceeded"

        const val TOKEN_EXPIRED = "error.token.expired"
        const val TOKEN_INCORRECT = "error.token.incorrect"
        const val TOKEN_MISSING= "error.token.missing"
        const val TOKEN_USED = "error.token.used"


        const val ADDRESS_NOT_FOUND = "error.address.notFound"
        const val ADDRESS_NOT_OWNER = "error.address.notOwner"


        const val OTP_INCORRECT = "error.otp.incorrect"
        const val EMAIL_NOT_SET = "error.account.emailNotSet"
        const val EMAIL_NOT_MATCHING = "error.account.emailNotMatching"
        const val EMAIL_NOT_NOT_VERIFIED = "error.account.emailNotSet"

        const val CANNOT_INVITE = "error.friends.cannot_invite"
        const val ALREADY_FIENDS = "error.friends.already_friends"
        const val NOT_FRIENDS = "error.friends.not_friends"

        const val CANNOT_SEND_MESSAGE = "error.messages.cannotSend"

        const val PERMISSION_DENIED = "error.general.permissionDenied"
        const val INTERNAL_ERROR = "error.general.internalError"
    }

    object Success {
        const val ACCOUNT_PASSWORD_CHANGED="success.account.passwordChanged"
        const val ACCOUNT_PASSWORD_RESET_LINK_SENT="success.account.passwordResetLinkSent"
    }

    object EmailSubject {
        const val ACCOUNT_PASSWORD_RESET = "email.subject.account.forgotPassword"
        const val ACCOUNT_VALIDATION = "email.subject.account.validation"
    }
}