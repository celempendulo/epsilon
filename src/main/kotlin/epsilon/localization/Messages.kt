package epsilon.localization

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.context.support.MessageSourceAccessor
import org.springframework.stereotype.Component
import java.util.*
import javax.annotation.PostConstruct

@Component
class Messages {
    @Autowired
    private lateinit var messageSource: MessageSource
    private lateinit var accessor: MessageSourceAccessor

    @PostConstruct
    fun init() {
        accessor = MessageSourceAccessor(messageSource, Locale.ENGLISH)
    }

    operator fun get(code: String): String {
        return get(code, code)
    }

    fun get(code: String, default: String): String {
        return accessor.getMessage(code, default)
    }


}