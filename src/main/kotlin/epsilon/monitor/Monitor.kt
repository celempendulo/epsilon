package epsilon.monitor

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import java.io.IOException
import java.io.PrintStream
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes


@EnableConfigurationProperties(MonitorProperties::class)
@Component
class Monitor @Autowired constructor(private val monitorProperties: MonitorProperties) {

    private var isMonitoring = true
    private val path: Path = Paths.get(monitorProperties.target)

    init {
        if(Files.isDirectory(path)) {
            monitor()
        }
        else {
            isMonitoring = false
        }
    }

    private fun monitor() {
        val service = path.fileSystem.newWatchService()
        Files.walkFileTree(path, WatcherVisitor(service))

        var key: WatchKey
        while(service.take().also { key = it} != null) {
            Thread.sleep((1000 * monitorProperties.wait).toLong())
            for(command in monitorProperties.commands) {
                val process = run(command)
                process.waitFor()
                PrintStream(process.outputStream).println()
            }
            key.pollEvents()
            key.reset()
        }
    }

    private fun run(command: String): Process {
        val builder = ProcessBuilder()
        builder.command(command.split(" "))
                .directory(path.toFile())
        return builder.start()
    }

    class WatcherVisitor(private val service: WatchService): SimpleFileVisitor<Path>() {

        override fun visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult {
            return FileVisitResult.CONTINUE
        }

        override fun postVisitDirectory(dir: Path, exc: IOException): FileVisitResult {
            dir.register(service,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_MODIFY,
                    StandardWatchEventKinds.ENTRY_DELETE)
            return FileVisitResult.CONTINUE
        }
    }

}