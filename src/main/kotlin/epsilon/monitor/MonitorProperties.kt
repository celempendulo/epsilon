package epsilon.monitor

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "epsilon.monitor")
class MonitorProperties(val target: String = "",
                        val assemble: Boolean = true,
                        val wait: Int = 10,
                        val commands: Array<String> = arrayOf("gradle assemble") )