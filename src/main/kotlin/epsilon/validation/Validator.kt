package epsilon.validation


object Validator {

    private val passwordRegex = Regex("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,40}$")
    private val emailRegex = Regex("""^[\w-+]+(\\.[\\w]+)*@[\w-]+(\.[\w]+)*(\.[a-z]{2,})$""")
    private val handlerRegex = Regex("""^(?!.*\.\.)(?!.*\.${'$'})[^\W][\w.]{5,29}""")

    /**
     * Check is a password is valid using a regular expression
     * @param password
     * @param regex regular expression used to validate password,
     *          epsilon has a default regular expression to validate passwords
     */
    fun isValidPassword(password: String, regex:Regex= passwordRegex): Boolean {
        return password.matches(regex)
    }


    /**
     * Check is an email address is valid using a regular expression
     */
    fun isValidEmail(email: String, regex: Regex= emailRegex): Boolean {
        return regex.matches(email)
    }

    fun isValidHandler(handler: String, regex: Regex= handlerRegex): Boolean {
        return regex.matches(handler)
    }

}