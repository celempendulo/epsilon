package epsilon.keys

import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.file.Files
import java.nio.file.Paths
import java.security.Key
import java.security.KeyStore
import java.security.KeyStore.PasswordProtection
import java.security.KeyStoreException
import java.util.*

@Component
@EnableConfigurationProperties(KeyStoreProperties::class)
class CustomKeyStore @Autowired constructor(
        private val properties: KeyStoreProperties) {

    private val keyStore: KeyStore
    private val protection: PasswordProtection

    fun getKey(alias: String): Key? {
        return keyStore.getKey(alias, protection.password)
    }

    fun addKey(alias: String, key: Key) {
        keyStore.setKeyEntry(alias, key, protection.password, null)
        keyStore.store(FileOutputStream(properties.file), protection.password)
    }

    fun containsAlias(alias: String): Boolean {
        try {
            return keyStore.containsAlias(alias)
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        }
        return false
    }

    fun generate(alias: String): Key {
        return generate(alias, SignatureAlgorithm.HS256)
    }

    fun generate(alias: String, signatureAlgorithm: SignatureAlgorithm): Key {
        val key: Key = Keys.secretKeyFor(signatureAlgorithm)
        addKey(alias, key)
        return key
    }

    fun getAliases(): Enumeration<String> {
        return keyStore.aliases()
    }

    init {
        val password = properties.password.toCharArray()
        protection = PasswordProtection(password)
        keyStore = KeyStore.getInstance(properties.type)
        if (Files.exists(Paths.get(properties.file))) {
            keyStore.load(FileInputStream(properties.file), protection.password)
        } else {
            Files.createFile(Paths.get(properties.file))
            keyStore.load(null, password)
            keyStore.store(FileOutputStream(properties.file), password)
        }
    }
}