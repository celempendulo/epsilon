package epsilon.keys.repository

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.keys.KeyInformation

interface KeyRepository : ArangoRepository<KeyInformation, String> {
}