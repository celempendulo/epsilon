package epsilon.keys

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "epsilon.keystore")
class KeyStoreProperties(val file: String, val type: String, val password: String)