package epsilon.keys

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id
import java.util.*

@Document("keys")
data class KeyInformation(@Id val alias:String,
                          val algorithm:String,
                          val createdOn: Date= Date()) : Comparable<KeyInformation> {
    override fun compareTo(other: KeyInformation): Int {
        return this.createdOn.compareTo(other.createdOn)
    }
}