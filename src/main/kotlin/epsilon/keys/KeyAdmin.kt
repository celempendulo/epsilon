package epsilon.keys

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import epsilon.keys.repository.KeyRepository
import io.jsonwebtoken.SignatureAlgorithm
import java.security.Key
import java.time.Instant
import javax.crypto.spec.SecretKeySpec

@Component
class KeyAdmin @Autowired constructor(
        private val keyStore: CustomKeyStore,
        private val keyRepository: KeyRepository) {

    private var signingKeyInfo: KeyInformation? = null
    private var signingKey: Key? = null

    fun init() {
        val all = keyRepository.findAll().toSortedSet()
        if (!all.isEmpty()) {
            signingKeyInfo = all.first()
            signingKey = SecretKeySpec(keyStore.getKey(signingKeyInfo!!.alias)!!.encoded, signingKeyInfo!!.algorithm)
        } else {
            generate()
        }
    }

    fun getKey(): Key{
        if(signingKey == null){
            init()
        }
        return signingKey!!
    }

    fun getKeyInformation(): KeyInformation {
        if(signingKeyInfo == null){
            init()
        }
        return signingKeyInfo!!
    }

    private fun generate() {
        val alias = Instant.now().epochSecond.toString()
        val information = KeyInformation(alias = alias, algorithm = SignatureAlgorithm.HS256.jcaName)
        signingKey = SecretKeySpec(keyStore.generate(alias, SignatureAlgorithm.HS256).encoded, information.algorithm)
        keyRepository.save(information)
        signingKeyInfo = information
    }

    fun getKey(alias: String): Key? {
         if(signingKeyInfo?.alias==alias) {
             return signingKey
         }
        val info = keyRepository.findById(alias)
        if(info.isPresent) {
            println(keyStore.getKey(alias)?.algorithm)
            return SecretKeySpec(keyStore.generate(alias, SignatureAlgorithm.HS256).encoded, info.get().algorithm)
        }
        return null

    }
}