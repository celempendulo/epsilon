package epsilon.otp

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import org.springframework.data.annotation.Id
import epsilon.accounts.Account

@Edge(value = "otpOwner")
data class OtpOwner(@From var owner: Account, @To var otp: Otp, @Id var id: String?=null)