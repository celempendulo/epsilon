package epsilon.otp

import com.arangodb.springframework.annotation.Document
import com.arangodb.springframework.annotation.Relations
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.data.annotation.Id
import epsilon.accounts.Account
import java.util.*

@JsonIgnoreProperties("value", "owner", "id")
@Document("otp")
data class Otp(var subject: String,
               var value: String,
               var expireOn: Date,
               @Id var id: String?=null,
               var createdOn: Date= Date(),
               var used: Boolean =false) {

    @Relations(edges = [OtpOf::class])
    var owner: Account? = null

    companion object {
        @JvmOverloads
        fun generate(length: Int = 6): String {
            val builder = StringBuilder()
            for (i in 0 until length) {
                builder.append((Math.random() * 10).toInt())
            }
            return builder.toString()
        }
    }

}