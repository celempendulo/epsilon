package epsilon.otp.data

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.otp.OtpOwner

interface OtpOfRepository : ArangoRepository<OtpOwner, String>