package epsilon.otp.data

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.otp.Otp
import java.util.*

interface OtpRepository : ArangoRepository<Otp, Long> {
    fun findBySubjectAndOwnerEmail(subject: String, email: String): Optional<Otp>
}