package epsilon.otp

import epsilon.ApplicationProperties
import epsilon.accounts.Account
import epsilon.failure.ClientError
import epsilon.localization.Messages
import epsilon.localization.Tag
import epsilon.mail.CustomMailService
import epsilon.otp.data.OtpOfRepository
import epsilon.otp.data.OtpRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*

@Component
class OtpManager @Autowired constructor(
        private val messages: Messages,
        private val otpRepository: OtpRepository,
        private val otpOfRepository: OtpOfRepository,
        private val mailService: CustomMailService,
        private val properties: ApplicationProperties
) {

    fun send(subjectKey: String, owner:Account): Otp {
        var otp = Otp(
                subjectKey,
                Otp.generate(),
                Date())
        otp.owner = owner
        otp = otpRepository.save(otp)
        otpOfRepository.save(OtpOwner(owner, otp))

        mailService.send(
                owner.email!!,
                properties.email,
                messages[subjectKey],
                "Here is your one time password : " + otp.value
        )
        return otp
    }

    fun isCorrect(otpValue: String, owner: Account): Boolean {
        val otpContainer =
                otpRepository.findBySubjectAndOwnerEmail(Tag.EmailSubject.ACCOUNT_VALIDATION, owner.email!!)
        if (!otpContainer.isPresent || otpContainer.get().used) {
            throw ClientError(messages[Tag.Error.OTP_INCORRECT])
        }
        val otp = otpContainer.get()
        if (otp.value == otpValue) {
            otp.used = true
            otpRepository.save(otp)
            return true
        } else {
            otpRepository.save(otp)
            throw ClientError(messages[Tag.Error.ACCOUNT_INCORRECT_CREDENTIALS])
        }
    }

}