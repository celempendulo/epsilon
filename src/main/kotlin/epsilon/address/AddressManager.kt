package epsilon.address

import epsilon.accounts.AccountManager
import epsilon.failure.ClientError
import epsilon.localization.Messages
import epsilon.address.repository.AddressOwnerRepository
import epsilon.address.repository.AddressRepository
import epsilon.localization.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class AddressManager @Autowired constructor(
        private val accountManager: AccountManager,
        private val addressRepository: AddressRepository,
        private val addressOwnerRepository: AddressOwnerRepository,
        private val messages: Messages) {

    fun add(address: Address, email:String): Address {
        val account = accountManager.get(email).get()
        address.owner = account
        val result = addressRepository.save(address)
        addressOwnerRepository.save(AddressOwner(account, result))

        return result
    }

    fun update(id:String, address: Address, email: String): Address {
        val oldAddress = validateOwnership(id, email)
        address.id = oldAddress.id
        return addressRepository.save(address)
    }

    fun delete(id: String, email: String): Address {
        val address = validateOwnership(id, email)
        addressRepository.delete(address)
        return address
    }

    fun get(email: String): List<Address> {
        return addressRepository.findByOwnerEmail(email)
    }

    private fun validateOwnership(id:String, email: String): Address {
        val container = addressRepository.findById(id)
        if(!container.isPresent) throw ClientError(messages[Tag.Error.ADDRESS_NOT_FOUND])
        val address = container.get()
        if(address.owner?.email != email) {
            throw ClientError(messages[Tag.Error.ADDRESS_NOT_OWNER])
        }

        return address
    }
}