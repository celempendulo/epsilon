package epsilon.address

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import epsilon.accounts.Account
import org.springframework.data.annotation.Id


@Edge("addressOwner")
data class AddressOwner(@From val owner: Account,
                        @To val address: Address,
                        @Id val id:String?=null)