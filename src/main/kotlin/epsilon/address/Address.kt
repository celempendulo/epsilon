package epsilon.address

import com.arangodb.springframework.annotation.*
import epsilon.accounts.Account
import org.springframework.data.annotation.Id


@Document("address")
data class Address(val lines:List<String>,
                   val province:String,
                   val city:String,
                   val postalCode:Int,
                   @Relations(edges = [AddressOwner::class]) var owner: Account? = null,
                   @Id var id: String?=null)
