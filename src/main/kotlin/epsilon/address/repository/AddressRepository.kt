package epsilon.address.repository

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.address.Address

interface AddressRepository : ArangoRepository<Address, String>{
    fun findByOwnerEmail(email:String) : List<Address>
}