package epsilon.address.repository

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.address.AddressOwner

interface AddressOwnerRepository : ArangoRepository<AddressOwner, String>