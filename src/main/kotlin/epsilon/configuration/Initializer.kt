package epsilon.configuration

import com.arangodb.entity.CollectionType
import com.arangodb.model.CollectionCreateOptions
import com.arangodb.springframework.annotation.Document
import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.core.ArangoOperations
import epsilon.ApplicationProperties
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.core.type.filter.AnnotationTypeFilter
import org.springframework.stereotype.Component

@Suppress("unused")
@Component
class Initializer @Autowired constructor(
        private val operations: ArangoOperations,
        private val applicationProperties: ApplicationProperties) {

    fun init() {
        createDocuments()
        createEdges()
    }

    fun createEdges() {
        val scanner = ClassPathScanningCandidateComponentProvider(false)
        scanner.addIncludeFilter(AnnotationTypeFilter(Edge::class.java))

        scanner.findCandidateComponents("epsilon").forEach {
            val doc = Class.forName(it.beanClassName).getAnnotation(Edge::class.java)
            operations.collection(doc.value, CollectionCreateOptions().type(CollectionType.EDGES))
        }

        scanner.findCandidateComponents(applicationProperties.name).forEach {
            val doc = Class.forName(it.beanClassName).getAnnotation(Edge::class.java)
            val rs =operations.collection(doc.value, CollectionCreateOptions().type(CollectionType.EDGES))
        }

    }
    fun createDocuments(){
        val scanner = ClassPathScanningCandidateComponentProvider(false)
        scanner.addIncludeFilter(AnnotationTypeFilter(Document::class.java))

        scanner.findCandidateComponents("epsilon").forEach {
            val doc = Class.forName(it.beanClassName).getAnnotation(Document::class.java)
            operations.collection(doc.value)

        }

        scanner.findCandidateComponents(applicationProperties.name).forEach {
            val doc = Class.forName(it.beanClassName).getAnnotation(Document::class.java)
            operations.collection(doc.value)
        }
    }

}