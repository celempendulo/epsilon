package epsilon.configuration

import com.arangodb.springframework.boot.autoconfigure.ArangoAutoConfiguration
import com.arangodb.springframework.boot.autoconfigure.ArangoProperties
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.context.properties.EnableConfigurationProperties


@Suppress("unused")
@EnableAutoConfiguration
@EnableConfigurationProperties(ArangoProperties::class)
class DBConfiguration(arangoProperties: ArangoProperties): ArangoAutoConfiguration(arangoProperties)

