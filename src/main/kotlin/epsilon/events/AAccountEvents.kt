package epsilon.events

import com.arangodb.springframework.core.mapping.event.AbstractArangoEventListener
import com.arangodb.springframework.core.mapping.event.AfterSaveEvent
import com.arangodb.springframework.core.mapping.event.BeforeDeleteEvent
import epsilon.accounts.Account
import epsilon.accounts.AccountManager
import epsilon.tokens.TokenManager
import org.springframework.beans.factory.annotation.Autowired


abstract class AAccountEvents @Autowired constructor(
        private val tokenManager: TokenManager,
        private val accountManager: AccountManager) : AbstractArangoEventListener<Account>() {

    override fun onAfterSave(event: AfterSaveEvent<Account>?) {
    }

    override fun onBeforeDelete(event: BeforeDeleteEvent<Account>) {
        accountManager.get(event.source as String).ifPresent {
            tokenManager.deleteAll(it)
        }
    }
}