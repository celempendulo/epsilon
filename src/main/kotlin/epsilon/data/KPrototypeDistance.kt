package epsilon.data

import kotlin.math.pow
import kotlin.math.sqrt

class KPrototypeDistance: IClusterNode.IDistance {

    private var categoricalWeight = 1.0
    private var weights = mutableMapOf<Dimension, Double>()

    override fun setWeights(weights: Map<Dimension, Double>) {
        this.weights.putAll(weights)
    }

    override fun get(from: IClusterNode, to: IClusterNode): Double {

        return euclidean(from, to)+modes(from,to)
    }

    fun euclidean(from: IClusterNode, to: IClusterNode): Double {
        var sum = 0.0
        from.getValues().keys.filter { it.type == Dimension.DataType.NUMERIC }.forEach {
            val first = from.getValues()[it]
            val second = to.getValues()[it]
            if(first != null || second != null) {
                val weight = weights.getOrDefault(it,1.0)
                sum += weight*(first!! as Double - second!! as Double).pow(2)
            }
        }
        return sqrt(sum)
    }

    fun modes(from: IClusterNode, to: IClusterNode): Double {
        var sum = 0.0
        from.getValues().keys.filter { it.type == Dimension.DataType.CATEGORICAL }.forEach {
            val first = from.getValues()[it]
            val second = to.getValues()[it]
            sum += if(first == second) 0.0 else weights.getOrDefault(it, 1.0)
        }
        return sum
    }


}