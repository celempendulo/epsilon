package epsilon.data

class Cluster (var center: IClusterNode, val nodes: MutableList<IClusterNode> = mutableListOf()) {

    fun updateCenter(): Boolean {
        return updateCenterNumeric() && updateCenterCategorical()
    }

    fun getWithinDistance(distance: IClusterNode.IDistance = KPrototypeDistance()): Double {
        var sum = 0.0
        for (node in nodes) {
            sum += distance.get(center, node)
        }
        return sum
    }

    private fun updateCenterNumeric(): Boolean {
        val numeric = mutableMapOf<Dimension, MutableList<Double>>()
        for(dim in center.getValues().keys) {
            if(dim.type == Dimension.DataType.NUMERIC) {
                numeric[dim] = mutableListOf()
            }
        }
        for(node in nodes) {
            for(entry in node.getValues()) {
                if(entry.key.type == Dimension.DataType.NUMERIC) {
                    numeric[entry.key]!!.add(entry.value as Double)
                }
            }
        }
        var changed = false
        for (entry in center.getValues()) {
            if(entry.key.type == Dimension.DataType.NUMERIC) {
                val average =  numeric[entry.key]!!.average().toString()
                if(average != center.getValues()[entry.key]) {
                    center.getValues()[entry.key] = average
                    changed = true
                }
            }
        }
        return changed
    }

    private fun updateCenterCategorical(): Boolean {
        val categorical = mutableMapOf<Dimension, MutableList<String>>()
        for(dim in center.getValues().keys) {
            if(dim.type == Dimension.DataType.CATEGORICAL) {
                categorical[dim] = mutableListOf()
            }
        }
        for(node in nodes) {
            for(entry in node.getValues()) {
                if(entry.key.type == Dimension.DataType.CATEGORICAL){
                    categorical[entry.key]!!.add(entry.value as String)
                }
            }
        }
        var changed = false
        for (entry in center.getValues()) {
             if(entry.key.type == Dimension.DataType.CATEGORICAL) {
                val mode = categorical[entry.key]!!.groupingBy { it }.eachCount().maxBy { it.value }!!.key
                if(mode != center.getValues()[entry.key]) {
                    center.getValues()[entry.key] = mode
                    changed = true
                }
            }
        }
        return changed
    }
}