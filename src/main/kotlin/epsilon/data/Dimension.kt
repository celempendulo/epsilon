package epsilon.data

data class Dimension(val name: String, val type: DataType=DataType.NUMERIC) {

    enum class DataType {
        NUMERIC,
        CATEGORICAL
    }
}