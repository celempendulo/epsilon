package epsilon.data


interface IClusterNode {

    fun getValues(): MutableMap<Dimension, Any>



    interface IDistance {

        fun setWeights(weights: Map<Dimension, Double>) {

        }

        fun get(from: IClusterNode, to: IClusterNode): Double

    }
}