package epsilon.data

import org.springframework.stereotype.Component
import java.lang.IllegalArgumentException
import java.util.*
import kotlin.math.PI
import kotlin.math.atan

@Component
class ClusterUtils {


    /**
     * Finds a knee in the the within distance graph by analyzing the angle between every two joined lines
     */
    fun getNumberOfClusters(items: List<IClusterNode>, distanceAlg: IClusterNode.IDistance = KPrototypeDistance(),
                            min:Int = 2, max:Int = items.size/2): Int {

        if(min <=0 || max <= 0 || max-min < 2 || items.size < 3.coerceAtLeast(max)) {
            throw IllegalArgumentException("Invalid input data")
        }
        val map: TreeMap<Int, Double> = TreeMap()
        for(k in min until max+1) {
            val clusters = cluster(items, distanceAlg, k)
            var sum = 0.0
            for(cluster in clusters) {
                sum += cluster.getWithinDistance(distanceAlg)
            }
            map[k] = sum
        }
        var angle = 10.0
        var k = 0
        for(index in min until max-1) {
            val firstAngle = atan(map[index+1]!! - map[index]!!)+ PI/2
            val secondAngle = -atan(map[index+2]!!-map[index+1]!!)
            if(angle >= firstAngle+secondAngle) {
                angle = firstAngle+secondAngle
                k = index+1
            }
        }

        return k
    }

    fun cluster(items: List<IClusterNode>, distanceAlg: IClusterNode.IDistance = KPrototypeDistance(),
                k:Int = getNumberOfClusters(items, distanceAlg)): List<Cluster> {
        val clusters = mutableListOf<Cluster>()
        for(index in 1..k) {
            clusters.add(Cluster(items[index]))
        }
        var done: Boolean
        do {
            for (node in items) {
                var cluster = clusters[0]
                var distance = distanceAlg.get(cluster.center, node)
                for(index in 2..k) {
                    distanceAlg.get(clusters[index].center, node).let {
                        if(it > distance) {
                            cluster = clusters[index]
                            distance = it
                        }
                    }
                }
                cluster.nodes.add(node)
            }
            done = true
            for(cluster in clusters) {
                done = done && !cluster.updateCenter()
            }

        }while(!done)

        return clusters
    }
}