package epsilon.tokens

import com.arangodb.springframework.annotation.Document
import com.arangodb.springframework.annotation.Relations
import epsilon.accounts.Account
import org.springframework.data.annotation.Id
import java.time.LocalDateTime
import java.util.UUID

@Document("tokens")
data class Token(
        val expireAt: LocalDateTime=LocalDateTime.now().plusMinutes(30),
        val createdAt: LocalDateTime = LocalDateTime.now(),
        var used: Boolean = false,
        var overriden: Boolean = false,
        var type: Type,
        val code: String= UUID.randomUUID().toString(),
        @Relations(edges = [TokenOwner::class]) var owner: Account? = null,
        @Id var id: String? = null) {

    enum class Type {
        PASSWORD_TOKEN,
        EMAIL_CONFIRMATION
    }
}