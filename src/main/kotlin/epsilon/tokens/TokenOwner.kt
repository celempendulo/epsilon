package epsilon.tokens

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import epsilon.accounts.Account
import org.springframework.data.annotation.Id

@Edge("tokenOwner")
class TokenOwner(@From val owner: Account, @To val token: Token , @Id var id: String?=null)