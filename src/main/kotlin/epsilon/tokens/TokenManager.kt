package epsilon.tokens

import epsilon.accounts.Account
import epsilon.failure.ClientError
import epsilon.localization.Messages
import epsilon.localization.Tag
import epsilon.tokens.repository.TokenOwnerRepository
import epsilon.tokens.repository.TokenRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.util.*

@Component
class TokenManager @Autowired constructor(
        private val repository: TokenRepository,
        private val ownerRepository: TokenOwnerRepository,
        private val messages: Messages) {


    fun add(owner: Account, type: Token.Type): Token {
        var token = Token(type = type)
        repository.findByOwnerIdAndTypeAndUsedAndOverriden(owner.id!!, token.type).forEach {
            it.overriden = true
            repository.save(it)
        }
        token = repository.save(token)
        ownerRepository.save(TokenOwner(owner, token))
        return token
    }

    fun get(account: Account, type:Token.Type): Optional<Token> {
        val tokens = repository.findByOwnerIdAndTypeAndUsedAndOverriden(account.id!!, type)
        if(tokens.isEmpty()){
            return Optional.empty()
        }
        return Optional.of(tokens[0])
    }

    fun deleteAll(account: Account) {
        ownerRepository.findByOwnerId(account.id!!).ifPresent {
            repository.delete(it.token)
            ownerRepository.delete(it)
        }
    }

    fun markAsUsed(id: String) {
        repository.findById(id).ifPresent {
            it.used = true
            repository.save(it)
        }
    }

    fun validate(owner: Account, type: Token.Type, code: String){
        val container = get(owner, type)
        if(!container.isPresent) throw ClientError(messages[Tag.Error.TOKEN_MISSING])
        val token = container.get()
        if(token.used) {
            throw ClientError(Tag.Error.TOKEN_USED)
        }
        if(token.expireAt.isBefore(LocalDateTime.now())) {
            throw ClientError(messages[Tag.Error.TOKEN_EXPIRED])
        }

        if(token.code != code) {
            token.used = true
            repository.save(token)
            throw ClientError(messages[Tag.Error.TOKEN_INCORRECT])
        }
    }


}