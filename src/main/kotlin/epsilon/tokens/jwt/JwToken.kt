package epsilon.tokens.jwt

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id
import java.util.*

@Document("jwt")
data class JwToken(@Id val jwt: String, val exp: Date)