package epsilon.tokens.jwt.data

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.tokens.jwt.JwToken

interface BlackListedTokenRepository : ArangoRepository<JwToken?, String?>