package epsilon.tokens.jwt

import epsilon.ApplicationProperties
import io.jsonwebtoken.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import epsilon.keys.KeyAdmin
import epsilon.tokens.jwt.data.BlackListedTokenRepository
import org.springframework.boot.context.properties.EnableConfigurationProperties
import java.security.Key
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

@Component
@EnableConfigurationProperties(ApplicationProperties::class)
class TokenAdmin @Autowired constructor(
        private val keyAdmin: KeyAdmin,
        private val applicationProperties: ApplicationProperties,
        private val tokens: BlackListedTokenRepository) : SigningKeyResolverAdapter() {


    fun generateToken(email: String): String {
        val exp =
            Date.from(LocalDateTime.now().plusDays(10).atZone(ZoneId.systemDefault()).toInstant())
        return Jwts.builder().setHeaderParam(JwsHeader.KEY_ID, keyAdmin.getKeyInformation().alias)
                .setIssuedAt(Date()).setExpiration(exp).setIssuer(applicationProperties.name).setSubject(email)
                .signWith(keyAdmin.getKey()).compact()
                //.signWith(keyAdmin.key, SignatureAlgorithm.forName(keyAdmin.keyInformation.algorithm)).compact()
    }

    fun isBlacklisted(jwt: String): Boolean {
        return tokens.existsById(jwt)
    }

    fun blacklist(jwt: String) {
        try {
            val values = getClaims(jwt)
            val date = values.body.expiration
            tokens.save(JwToken(jwt, date))
        } catch (e: Exception) { // token is invalid
            e.printStackTrace()
        }
    }

    override fun resolveSigningKey(header: JwsHeader<*>, claims: Claims): Key? {
        val alias = header.getKeyId() ?: return null
         return keyAdmin.getKey(alias)
    }

    fun getClaims(jwt: String): Jws<Claims> {
        return Jwts.parser().setSigningKeyResolver(this).requireIssuer(applicationProperties.name).parseClaimsJws(jwt)
    }
}