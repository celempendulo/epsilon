package epsilon.tokens.repository

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.tokens.TokenOwner
import java.util.*

interface TokenOwnerRepository: ArangoRepository<TokenOwner, String> {

    fun deleteByOwnerId(id: String)
    fun findByOwnerId(id: String): Optional<TokenOwner>
}