package epsilon.tokens.repository

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.accounts.Account
import epsilon.tokens.Token

interface TokenRepository: ArangoRepository<Token, String> {

    fun findByOwnerIdAndTypeAndUsedAndOverriden(id:String, type:Token.Type, used:Boolean=false,
                                              overriden:Boolean=false): List<Token>

}