package epsilon.media

enum class ImageFormat {
    PNG,
    JPEG,
    JPG
}