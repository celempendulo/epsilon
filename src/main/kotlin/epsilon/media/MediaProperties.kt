package epsilon.media

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "epsilon.media")
class MediaProperties(val audioFormat:MediaFormat = MediaFormat.OPUS,
                      val videoFormat:MediaFormat = MediaFormat.MP4,
                      val folderName:String = "media",
                      val maxSize:Int= 10)