package epsilon.media

enum class MediaFormat {
    MP3,
    MP4,
    WAV,
    OPUS
}