package epsilon.media

import epsilon.ApplicationProperties
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.util.DigestUtils
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

@Suppress("unused")
@Component
@EnableConfigurationProperties(ApplicationProperties::class, MediaProperties::class)
class MediaManager @Autowired constructor(
        applicationProperties: ApplicationProperties,
        private val mediaProperties: MediaProperties) {

    private var directory: Path = Paths.get(applicationProperties.mainDirectory).resolve(mediaProperties.folderName)


    fun convert(inputStream: InputStream, inputFormat: MediaFormat,
                outputFormat: MediaFormat = mediaProperties.audioFormat): InputStream {

        if(inputFormat == outputFormat){
            return inputStream
        }
        val input = UUID.randomUUID().toString() +"."+ inputFormat.name.toLowerCase()
        val output = input.replaceAfterLast(".",outputFormat.name.toLowerCase())
        Files.write(directory.resolve(input), inputStream.readAllBytes())
        runCommand(listOf("opusenc", input, output))

        val outputPath = directory.resolve(output)
        val stream =  ByteArrayInputStream(Files.readAllBytes(outputPath))

        Files.delete(directory.resolve(input))
        Files.delete(directory.resolve(output))
        return stream
    }

    fun makeVideo(audio: InputStream, audioFormat: MediaFormat, image: InputStream, imageFormat: ImageFormat,
                  outputFormat: MediaFormat = mediaProperties.videoFormat): InputStream {

        val input = UUID.randomUUID().toString() +"."+ audioFormat.name.toLowerCase()
        val pic = input.replaceAfterLast(".", imageFormat.name.toLowerCase())
        val output = input.replaceAfterLast(".", outputFormat.name.toLowerCase())

        Files.write(directory.resolve(input), audio.readAllBytes())
        Files.write(directory.resolve(pic), image.readAllBytes())
        runCommand(listOf("opusenc", "--picture", pic, input, output))

        val outputPath = directory.resolve(output)
        val stream =  ByteArrayInputStream(Files.readAllBytes(outputPath))

        Files.delete(directory.resolve(input))
        Files.delete(directory.resolve(output))
        Files.delete(directory.resolve(pic))
        return stream
    }

    fun getMD5(inputStream: InputStream): String {
        return DigestUtils.md5DigestAsHex(inputStream)
    }


    private fun runCommand(command: List<String>): Process {

        val builder = ProcessBuilder()
        builder.command(command)
                .directory(directory.toFile())
        return builder.start()
    }

    fun getSize(file: MultipartFile): Long {
        return file.size/ 1024L*1024L
    }
}