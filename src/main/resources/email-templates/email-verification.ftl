Hi ${RECEIVER},<br/>

Thanks for using ${APPLICATION_NAME}! Please confirm your email address by clicking on the link below.<br/>

${URL}<br/>

If you did not sign up to ${APPLICATION_NAME} account please disregard this email.<br/>

From ${APPLICATION_NAME} team