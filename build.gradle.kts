import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("maven")
	kotlin("jvm") version "1.4.10"
	kotlin("plugin.spring") version "1.4.10"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"}

group = "epsilon"
version = "1.0-SNAPSHOT"

repositories {
	mavenLocal()
	mavenCentral()
}

val developmentOnly: Configuration by configurations.creating
configurations {
	runtimeClasspath {
		extendsFrom(developmentOnly)
	}
}

dependencies {

	implementation(kotlin("stdlib-jdk8"))
	implementation(kotlin("reflect"))
	implementation("org.springframework.boot:spring-boot-starter-web:2.2.6.RELEASE")
	implementation("org.springframework.boot:spring-boot-starter-mail:2.2.4.RELEASE")
	implementation("com.okta.spring:okta-spring-boot-starter:1.4.0")
	implementation("nz.ac.waikato.cms.weka:weka-stable:3.8.4")
	implementation("com.arangodb:arangodb-spring-boot-starter:2.3.3.RELEASE")
	implementation("com.fasterxml.jackson.core:jackson-annotations:2.10.2")
	implementation("org.freemarker:freemarker:2.3.29")

	implementation("io.jsonwebtoken:jjwt-api:0.10.7")
	runtimeOnly("io.jsonwebtoken:jjwt-impl:0.10.7")
	runtimeOnly("io.jsonwebtoken:jjwt-jackson:0.10.7")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}



